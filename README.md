# MSL-backend

## 介绍
MSL是一款轻量级、高性能免费开源的快速开发平台。

- 前端基于Vue3、TypeScript、Pinia、Element Plus、Axios、echarts、dexie 、IndexedDB、Vite等技术栈。
- 后端基于Springboot2.7、Shiro、MyBatis-Plus、redisson、Swagger、EasyExcel、mapstruct、druid、异步、多线程、RBAC。等技术栈。
- 支持动态菜单、动态路由、无线层级菜单。
- 支持RBAC权限管理，权限控制到按钮。
- 自定义封装图文验证码组件
- 自定义封装svg图标选择组件
- 自定义封装安全校验组件
- 支持数据权限控制，可精确到特定的部门、特定的菜单。
- 支持文件分片并发上传、断点续传、秒传、文件在线预览、传输列表支持暂停、启动、重试等功能，而且刷新页面后传输列表会自动启动上传任务。
- 支持文件分片下载、断点续传、传输列表支持暂停、启动、重试等功能，而且刷新页面后传输列表会自动启动下载任务。
- 支持七牛云、本地服务。
- 支持三方社交账号登录(Gitee、Github、飞书、Oschina、钉钉、微博)。
- 异步任务中心
- 支持短信、邮箱验证码登录注册。
- 前端工程：https://gitee.com/mingsl/msl-frontend
- 开发文档 ：[https://zqertul28s1.feishu.cn/wiki/space/7367627537102979075?ccm_open_type=lark_wiki_spaceLink&open_tab_from=wiki_home](https://zqertul28s1.feishu.cn/wiki/space/7367627537102979075?ccm_open_type=lark_wiki_spaceLink&open_tab_from=wiki_home)

## 演示图

<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-ce1b06ffc1f85e5c15d855e589adb91d394.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4da5a2d958438702221f9812cf7a2fc4d39.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-6f45c17ebdc4b5c7289e3c7da9c5f187d7e.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-7ee5047d51aa425f8475ed39a759e92a0ff.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-71ab1e1336e86d584396d8f093b2fc981c6.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-612d629eab9ffe9a2c13c0c236be56787d5.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-692a36d7d2cd55cd7209f566f00c22871b7.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-18436f54bd584671932232c8b869974774b.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-a560dea3a475dc0155470bd1182227319b2.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-200e7f7e18f5ba4e951b0e36bce1b7fdfec.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-314ea21217f632654c9ddb5f3ec9d851290.png"/></td>
         <td><img src="https://oscimg.oschina.net/oscnet/up-d366896d1e5c7e128f3f9fb9a55260b0642.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-3247da838840c3d852e18788c424f41281f.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-bd348a415af1c5f5ea86590ecff84a0bb9f.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-e629c77662801e2d313b5a561a5e0907701.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-681cc112d52c77731d689475e9e8e8761a4.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-26f4b44a6e1951fc49365919a63a522eac8.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-65846a1f693389c49a3b21fabd45fa809c5.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4b565ac636ddf12ccdfe5eea8211b78198d.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-32d3f2ceab51aaed73a7151a5add5e46b43.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-82a633a5d4adab105b211eecbb0e1bd30c0.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-153fb5e9c4675210b3397a37b10f19f74f4.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c46494bc89e10579982cb42b105dd139aab.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-ba26b8ccc99eba63ff0f33978e70aa80940.png"/></td>
    </tr>
        <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-6458cc9a501b3dc059310a76c7c66a076cf.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-988ea50688cff1e896a618ae2f48a26b938.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-51451ea68792c6bbabb608e87001642b94b.png"/></td>
    </tr>
            <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-2b1e3e70e50862dbfaf954a7e9d1d0119a5.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c0e19831b2cf3e10c9d7e4e463db5f24ca4.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-42e72fb3b7abf312935730c901fad47ce3b.png"/></td>
    </tr>
</table>

## 联系我们加入组织

我们会用心回答解决大家在项目中使用的问题









