package com.msl.backend.common.security;

import org.apache.shiro.authc.AuthenticationToken;

public class AuthToken implements AuthenticationToken {
    private final String token;
    private final Long userId;

    public AuthToken(String token, Long userId) {
        this.token = token;
        this.userId=userId;
    }

    @Override
    public Object getPrincipal() {
        return userId;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
