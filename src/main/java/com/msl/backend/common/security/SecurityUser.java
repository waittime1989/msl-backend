package com.msl.backend.common.security;

import com.msl.backend.common.base.UserInfoDTO;
import com.msl.backend.common.constants.SystemConstants;
import com.msl.backend.common.enums.SuperAdminEnum;
import com.msl.backend.common.properties.SupperAccountProperties;
import com.msl.backend.common.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description: 用户安全信息工具类
 * @date: 2024/3/27
 */
@Component
@Slf4j
public class SecurityUser {
    @Resource
    private RedisUtils redisUtils;
    @Resource
    private SupperAccountProperties supperAccountProperties;
    /**
     * 获取用户信息
     */
    public UserInfoDTO getUserInfo() {
        UserInfoDTO userInfoDTO;
        try {

            userInfoDTO = redisUtils.getCacheObject(SystemConstants.getUserInfoCacheKey(getUserId()));
        }catch (Exception e){
            log.info("getUserInfo error :{}",e.getMessage());
            return null;
        }

        return userInfoDTO;
    }
    /**
     * 获取用户ID
     */
    public  Long getUserId() {
        try {
            return (Long) SecurityUtils.getSubject().getPrincipal();
        } catch (Exception e) {
            log.error("getUserId error:{}",e.getMessage());
            return null;
        }
    }

    /**
     * 获取请求token
     *
     * @param request
     * @return token
     */
    public String getToken(HttpServletRequest request)
    {
        String token = request.getHeader(SystemConstants.ACCESS_TOKEN);
        if (StringUtils.hasLength(token) && token.startsWith(SystemConstants.TOKEN_PREFIX))
        {
            token = token.replace(SystemConstants.TOKEN_PREFIX, "");
        }
        return token;
    }

    /**
     * 通过token 获取用户id
     * @param token
     * @return
     */
    public Long getUserIdByToken(String token){
        return redisUtils.getCacheObject(SystemConstants.getUserOnlineKey(token));
    }

    public boolean checkSuperAccount(){
        return supperAccountProperties.getAccount().stream().anyMatch(account -> account .equals( SecurityUtils.getSubject().getPrincipal().toString())) || SuperAdminEnum.YES.getValue().equals(getUserInfo().getUserType());
    }

}
