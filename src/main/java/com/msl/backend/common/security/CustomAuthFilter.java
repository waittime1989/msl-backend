package com.msl.backend.common.security;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.msl.backend.common.base.DataResult;
import com.msl.backend.common.constants.SystemConstants;
import com.msl.backend.common.exception.BusinessException;
import com.msl.backend.common.exception.ResponseCode;
import com.msl.backend.common.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * @ClassName: CustomAccessControlFilter
 * TODO:类文件简单描述
 * @Author: 小霍
 * @UpdateUser: 小霍
 * @Version: 0.0.1
 */
@Slf4j
public class CustomAuthFilter extends AccessControlFilter {
    @Resource
    private ObjectMapper objectMapper;
    @Resource
    private RedisUtils redisUtil;
    @Resource
    private SecurityUser securityUser;

    /**
     * 如果返回的是true 就流转到下一个链式调用
     * 返回false 就会流转到 onAccessDenied方法
     *
     * @param servletRequest
     * @param servletResponse
     * @param o
     * @return boolean
     * @throws
     * @Author: 小霍
     * @UpdateUser:
     * @Version: 0.0.1
     */
    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse, Object o) throws Exception {
        return false;
    }

    /**
     * 如果返true 就会流转到下一个链式调用
     * false 就是不会流转到下一个链式调用
     *
     * @param servletRequest
     * @param servletResponse
     * @return boolean
     * @throws
     * @Author: 小霍
     * @UpdateUser:
     * @Version: 0.0.1
     */
    @Override
    protected boolean onAccessDenied(ServletRequest servletRequest, ServletResponse servletResponse) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String accessToken = securityUser.getToken(request);
        log.info("url:{}", request.getRequestURI());

        try {
            //判断是否存在token
            if (!StringUtils.hasLength(accessToken)) {
                throw new BusinessException(ResponseCode.TOKEN_ERROR);
            }
            //判断token是否是系统有效签发
            if (!redisUtil.hasKey(SystemConstants.getUserOnlineKey(accessToken))) {
                throw new BusinessException(ResponseCode.TOKEN_EXP);
            }
            Long userId = securityUser.getUserIdByToken(accessToken);

            AuthToken jwtAuthToken = new AuthToken(accessToken, userId);
            getSubject(servletRequest, servletResponse).login(jwtAuthToken);
        } catch (BusinessException e) {
            customResponse(servletResponse, e.getCode(), e.getMessage());
            return false;
        } catch (AuthenticationException e) {
            if (e.getCause() instanceof BusinessException) {
                BusinessException exception = (BusinessException) e.getCause();
                customResponse(servletResponse, exception.getCode(), exception.getMessage());
            } else if (e instanceof UnknownAccountException) {
                customResponse(servletResponse, ResponseCode.TOKEN_ERROR.getCode(), e.getMessage());
            } else {
                customResponse(servletResponse, ResponseCode.TOKEN_ERROR.getCode(), ResponseCode.TOKEN_ERROR.getMessage());
            }
            return false;
        } catch (Exception e) {
            log.error("CustomAuthFilter onAccessDenied error:", e);
            customResponse(servletResponse, ResponseCode.SYSTEM_ERROR.getCode(), ResponseCode.SYSTEM_ERROR.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 自定义响应前端
     *
     * @param response
     * @param code
     * @param msg
     * @return void
     * @throws
     * @Author: 小霍
     * @UpdateUser:
     * @Version: 0.0.1
     */
    private void customResponse(ServletResponse response, int code, String msg) {
        DataResult<String> result = DataResult.fail(code, msg);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setCharacterEncoding("UTF-8");
        try(OutputStream outputStream  = response.getOutputStream()){
            outputStream.write(objectMapper.writeValueAsString(result).getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
        }catch (IOException exception){
            log.error("IOException:{}", exception.getMessage());
        }

    }


}
