package com.msl.backend.common.security;


import com.msl.backend.common.base.UserInfoDTO;
import com.msl.backend.common.constants.SystemConstants;
import com.msl.backend.common.utils.RedisUtils;

import com.msl.backend.test.entity.TestTable;
import com.msl.backend.test.service.TestTableService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import javax.annotation.Resource;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;

public class CustomRealm extends AuthorizingRealm {

    @Resource
    private TestTableService testTableService;

    @Resource
    private SecurityUser securityUser;
    @Resource
    private RedisUtils redisUtil;
    @Override
    protected boolean isPermitted(Permission permission, AuthorizationInfo info) {
        /**
         * 系统内部超级管理不需要去校验权限
         */
        if(securityUser.checkSuperAccount()){
            return true;
        }
        return super.isPermitted(permission, info);
    }

    @Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof AuthToken;
    }

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SimpleAuthorizationInfo info=new SimpleAuthorizationInfo();
        info.addStringPermissions(getUserPerms());
        return info;
    }
    /**
     * 脚手架搭建可以使用mock数据
     * 后面需要转换DB实际数据
     * @return
     */
    private List<String> getUserPerms(){
       return Arrays.asList("sys:user:query","sys:user:update","sys:user:delete");
    }
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        //编写判断逻辑
        Long userId = (Long) token.getPrincipal();
        UserInfoDTO infoDTO=getUserInfoDTO(userId);
        return new SimpleAuthenticationInfo(infoDTO.getId(),token.getCredentials(),CustomRealm.class.getName());
    }

    /**
     * 搭建脚手架的时候使用的是测试用户数据库(m_test_table)
     * 到实战项目的时候需要改成真正的用户数据库(m_user)
     * @param userId
     * @return
     */
    private UserInfoDTO getUserInfoDTO(Long userId){
        TestTable user =testTableService.getById(userId);
        if (user == null) {
            throw new  UnknownAccountException("账号不存在");
        }
        UserInfoDTO infoDTO= new UserInfoDTO();
        infoDTO.setId(user.getId());
        infoDTO.setNickname(user.getUsername());
        infoDTO.setGmtCreate(user.getGmtCreate());
        infoDTO.setGmtModified(user.getGmtModified());
        redisUtil.setCacheObject(SystemConstants.getUserInfoCacheKey(infoDTO.getId()),infoDTO, Duration.ofHours(SystemConstants.TOKEN_EXP));
        return infoDTO;
    }

}
