package com.msl.backend.common.annotation;
import com.msl.backend.common.validator.EnumValidator;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/7/24 16:49
 * @description: 自定义枚举校验器注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Constraint(validatedBy =EnumValidator.class)
public @interface EnumAnnotation {
    Class<?> value();
    String message() default "Invalid enum value";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}