package com.msl.backend.common.handler;
import com.msl.backend.common.base.DataResult;
import com.msl.backend.common.exception.BusinessException;
import com.msl.backend.common.exception.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/8/13 1:24
 * @description: 全局异常监控
 */
@RestControllerAdvice
@Slf4j
public class RestExceptionHandler {
    /**
     * 监控所有的 Exception异常
     * @param e
     * @param <T>
     * @return
     */
    @ExceptionHandler(Exception.class)
    public  <T> DataResult<T> handleException(Exception e){
        log.error("handleException e",e);
        return DataResult.fail(ResponseCode.SYSTEM_ERROR);
    }
    /**
     * 自定义全局异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(value = BusinessException.class)
    public <T> DataResult<T>  businessExceptionHandler(BusinessException e) {
        return DataResult.fail(e.getCode(),e.getMessage());
    }

    /**
     * 处理参数校验抛出的异常
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public <T> DataResult<T> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        log.error("methodArgumentNotValidExceptionHandler exception:", e);
        return DataResult.fail(ResponseCode.DATA_PARAM_ERROR.getCode(),
                e.getBindingResult().getAllErrors().stream()
                        .map(DefaultMessageSourceResolvable::getDefaultMessage)
                        .collect(Collectors.joining(", "))
        );
    }
    /**
     * 处理参数校验抛出的异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(ConstraintViolationException.class)
    public <T> DataResult<T> constraintViolationExceptionHandler(ConstraintViolationException e) {
        log.error("constraintViolationExceptionHandler exception:", e);
        return DataResult.fail(ResponseCode.DATA_PARAM_ERROR.getCode(),
                e.getConstraintViolations().stream()
                        .map(ConstraintViolation::getMessage)
                        .collect(Collectors.joining(", "))
        );
    }

    /**
     * 没有权限访问接口
     * 抛出的异常监控
     * @param e
     * @return
     * @param <T>
     */
    @ExceptionHandler(UnauthorizedException.class)
    public <T> DataResult<T> unauthorizedException(UnauthorizedException e){
        log.error("UnauthorizedException,{},{}",e.getLocalizedMessage(),e.getMessage());
        return DataResult.fail(ResponseCode.NOT_PERMISSION);
    }
}
