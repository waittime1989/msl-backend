package com.msl.backend.common.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/8/13 17:12
 * @description: mybatis plus 自动填充处理器
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("start insert fill ....");
        // 要与entity 实体类配置的自动填充字段对应
        this.strictInsertFill(metaObject, "gmtCreate", Date.class, new Date());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("start update fill ....");
        // 要与entity 实体类自动填充字段对应
        this.strictUpdateFill(metaObject, "gmtModified",  Date.class, new Date());
    }
}
