package com.msl.backend.common.exception;

import lombok.Getter;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date  2024/3/25
 * @description: 自定义的业务异常
 */
@Getter
public class BusinessException  extends RuntimeException{
    private final int code;
    private final String message;

    public BusinessException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }
    public BusinessException(ResponseCode responseCode) {
        super(responseCode.getMessage());
        this.code = responseCode.getCode();
        this.message = responseCode.getMessage();
    }
}