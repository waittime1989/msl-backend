package com.msl.backend.common.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/5/21
 */
@Getter
@AllArgsConstructor
public enum ResponseCode {
    SUCCESS(200,"操作成功"),
    //***************后端业务相关状态码**************
    SYSTEM_ERROR(50000,"系统异常,请稍后再试试"),
    //***************前端业务相关状态码**************
    DATA_PARAM_ERROR(40000,"传入参数错误"),
    TOKEN_ERROR(40101,"凭证错误，请重新登录"),
    TOKEN_EXP(40101,"token 过期，请重新登录"),
    NOT_PERMISSION(40301,"没有权限访问该资源"),
    ;

    /**
     *  响应码
     */
    private final int code;

    /**
     * 提示
     */
    private final String message;

}
