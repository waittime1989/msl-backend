package com.msl.backend.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/4/26
 */
@Configuration
@ConfigurationProperties(prefix = "super")
@Data
public class SupperAccountProperties {
    private List<String> account;
}
