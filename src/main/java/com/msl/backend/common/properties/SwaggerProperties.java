package com.msl.backend.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/5/21
 */
@Configuration
@ConfigurationProperties(prefix = "swagger")
@Data
public class SwaggerProperties {
    private boolean swagger2Enable;
}