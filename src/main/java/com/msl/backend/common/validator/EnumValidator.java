package com.msl.backend.common.validator;


import com.msl.backend.common.annotation.EnumAnnotation;
import com.msl.backend.common.base.BaseEnum;
import org.springframework.util.StringUtils;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Objects;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/8/13 14:37
 * @description: 枚举校验器
 */
public class EnumValidator implements ConstraintValidator<EnumAnnotation, Object> {
    private BaseEnum<Object>[] enumValues;

    @Override
    public void initialize(EnumAnnotation annotation) {
        enumValues = (BaseEnum<Object>[]) annotation.value().getEnumConstants();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (Objects.isNull(value) || !StringUtils.hasLength(value.toString())) {
            return true;
        }
        return Arrays.stream(enumValues).anyMatch(enumValue -> Objects.equals(enumValue.getValue(), value));
    }
}