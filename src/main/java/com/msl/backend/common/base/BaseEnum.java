package com.msl.backend.common.base;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/8/13 14:35
 * @description: 校验器枚举基类
 */
public interface BaseEnum <T>{
    T getValue();
}
