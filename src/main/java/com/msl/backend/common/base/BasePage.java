package com.msl.backend.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/8/13 17:24
 * @description: 分页查询基类
 */
@Data
public class BasePage {
    @ApiModelProperty(value = "当前页记录数")
    @Max(value = 1000,message = "最大查询1000条数据")
    private Integer size=10;
    @ApiModelProperty(value = "当前第几页")
    @NotNull(message = "当前页条数不能为空")
    private Integer current=1;
}
