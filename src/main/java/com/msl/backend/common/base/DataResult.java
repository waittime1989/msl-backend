package com.msl.backend.common.base;



import com.msl.backend.common.exception.ResponseCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2024/3/27
 * @description: 响应格式泛型类
 */
@Data
public class DataResult<T> {
    /**
     * 请求响应code，0为成功 其他为失败
     */
    @ApiModelProperty(value = "请求响应code，200为成功 其他为失败", name = "code")
    private int code = 200;

    /**
     * 响应异常码详细信息
     */
    @ApiModelProperty(value = "响应异常码详细信息", name = "message")
    private String message ;

    /**
     * 响应内容
     */
    @ApiModelProperty(value = "需要返回的数据", name = "data")
    private T data;

    /**
     * 响应成功的方法
     * @param <T>
     * @return
     */
    public static <T> DataResult<T> success(){
        DataResult<T> result=new DataResult<>();
        result.code= ResponseCode.SUCCESS.getCode();
        result.message=ResponseCode.SUCCESS.getMessage();
        return result;
    }

    /**
     * 响应成功的方法
     * @param data
     * @param <T>
     * @return
     */
    public static <T> DataResult<T> success(T data){
        DataResult<T> result=new DataResult<>();
        result.data=data;
        result.code= ResponseCode.SUCCESS.getCode();
        result.message=ResponseCode.SUCCESS.getMessage();
        return result;
    }

    /**
     * 响应失败工具方法
     * @param code
     * @param message
     * @param <T>
     * @return
     */
    public static <T> DataResult<T> fail(int code ,String message){
        DataResult<T> result=new DataResult<>();
        result.code= code;
        result.message=message;
        return result;
    }
    /**
     * 响应失败工具方法
     * @param responseCode
     * @param <T>
     * @return
     */
    public static <T> DataResult<T> fail(ResponseCode responseCode){
        DataResult<T> result=new DataResult<>();
        result.code= responseCode.getCode();
        result.message=responseCode.getMessage();
        return result;
    }
}