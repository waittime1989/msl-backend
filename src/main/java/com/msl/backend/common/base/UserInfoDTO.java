package com.msl.backend.common.base;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description: 用户信息表
 * @date: 2023/11/10
 */
@Data
public class UserInfoDTO implements Serializable {
    //id@TableId
    private Long id;
    //昵称
    private String nickname;
    //头像
    private String avatar;
    //性别   0：男   1：女   2：未知
    private Integer gender;
    //邮箱
    private String email;
    //手机号
    private String phone;
    //部门ID
    private Long deptId;
    //超级管理员   0：否   1：是
    private Integer userType;
    //状态  0：停用   1：正常
    private Integer status;
    //创建者
    private long creator;
    //创建时间
    private Date gmtCreate;
    //更新者
    private long updater;
    //更新时间
    private Date gmtModified;
}
