package com.msl.backend.common.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/8/13 17:26
 * @description:
 */
@Data
public class PageResult <T>{
    @ApiModelProperty(value = "当前页记录数")
    private List<T> list;
    @ApiModelProperty(value = "分页总数")
    private long total;

    public PageResult(List<T> list, long total) {
        this.list = list;
        this.total = total;
    }

    public static <T>PageResult<T> getPage(List<T> list,long total){
        return new PageResult<>(list,total);
    }
}
