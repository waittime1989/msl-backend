package com.msl.backend.common.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.servlet.Filter;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/3/28
 * @description:
 */
@Configuration
public class CorsWebConfig {
    @Bean
    public FilterRegistrationBean<CorsFilter> corsFilter(){
        UrlBasedCorsConfigurationSource source = getUrlBasedCorsConfigurationSource();
        FilterRegistrationBean<CorsFilter> filterRegistrationBean=new FilterRegistrationBean<>(new CorsFilter(source));
        /**
         * @description 配置优先级，因为后面我们讲到shiro其它的过滤器的时候要保证我们
         * 跨域过滤器优先级排在前面
         * @author 明思梨
         * @date 2023/3/28
         */
        filterRegistrationBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return filterRegistrationBean;
    }

    private static UrlBasedCorsConfigurationSource getUrlBasedCorsConfigurationSource() {
        UrlBasedCorsConfigurationSource source=new UrlBasedCorsConfigurationSource();
        CorsConfiguration configuration=new CorsConfiguration();
         //允许所有域名跨域访问
         //我们可以指定放开某个域名跨域访问
        configuration.addAllowedOrigin("*");
        //允许所有请求头跨域访问
        configuration.addAllowedHeader("*");
        //允许所有请求方法跨域访问
        configuration.addAllowedMethod("*");
        source.registerCorsConfiguration("/**",configuration);
        return source;
    }

}
