package com.msl.backend.common.config;

import com.msl.backend.common.properties.SwaggerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/5/21
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Resource
    private SwaggerProperties swaggerProperties;
    @Bean
    public Docket createRestApi() {
        //模拟请求头输入
        List<Parameter> pars = new ArrayList<>();
        ParameterBuilder tokenPar = new ParameterBuilder();
        tokenPar.name("Authorization").description("swagger测试用(模拟authorization传入)非必填 header").modelRef(new ModelRef("string")).parameterType("header").required(false);
        /**
         * 多个的时候 就直接添加到 pars 就可以了
         */
        pars.add(tokenPar.build());
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())//创建该Api的基本信息（这些基本信息会展现在文档页面中）
                .select()//函数返回一个ApiSelectorBuilder实例用来控制哪些接口暴露给Swagger ui来展现
                .apis(RequestHandlerSelectors.basePackage("com.msl.backend"))//指定需要扫描的包路路径
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(pars)
                .enable(swaggerProperties.isSwagger2Enable())
                ;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("MSL平台")
                .description("MSL-Api 接口文档")
                .termsOfServiceUrl("")
                .version("1.0")
                .build();
    }
}