package com.msl.backend.common.constants;

import java.text.MessageFormat;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/6/2
 */
public class SystemConstants {
    /**
     * token 权鉴key
     */
    public static final String ACCESS_TOKEN="Authorization";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 用户信息缓存key
     */
    public static final String USER_INFO_KEY="msl:user-info-cache";
    public static String getUserInfoCacheKey(Long userId){
        return MessageFormat.format("{0}:{1}",USER_INFO_KEY,String.valueOf(userId));
    }

    /**
     * 记录在线用户key
     */
    public static final Integer TOKEN_EXP=1;
    public static final String USER_ONLINE_KEY="msl:user-online-key";
    public static String getUserOnlineKey(String token){
        return MessageFormat.format("{0}:{1}",USER_ONLINE_KEY,token);
    }


}
