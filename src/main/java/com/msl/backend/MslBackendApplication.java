package com.msl.backend;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan({"com.msl.backend.*.dao"})
public class MslBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(MslBackendApplication.class, args);
    }

}
