package com.msl.backend.test.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 测试用户表(TestTable)表实体类
 *
 * @author 明思梨: https://space.bilibili.com/486686697
 * @since 2024-06-03 15:23:34
 */
@Data
@TableName("m_test_table")
public class TestTable {


    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("账户名称")
    private String username;

    @ApiModelProperty("用户密码")
    private String password;

    @ApiModelProperty("创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreate;

    @ApiModelProperty("更新时间")
    @TableField(fill = FieldFill.UPDATE)
    private Date gmtModified;



}
