package com.msl.backend.test.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.msl.backend.common.base.PageResult;
import com.msl.backend.common.constants.SystemConstants;
import com.msl.backend.common.exception.BusinessException;
import com.msl.backend.common.exception.ResponseCode;
import com.msl.backend.common.utils.RedisUtils;
import com.msl.backend.test.converter.TestTableConverter;
import com.msl.backend.test.dao.TestTableDao;
import com.msl.backend.test.entity.TestTable;
import com.msl.backend.test.model.dto.TestLoginDTO;
import com.msl.backend.test.model.dto.TestTablePageDTO;
import com.msl.backend.test.model.vo.TestLoginVO;
import com.msl.backend.test.model.vo.TestTableVO;
import com.msl.backend.test.service.TestTableService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.UUID;

/**
 * 测试用户表(TestTable)表服务实现类
 *
 * @author 明思梨: https://space.bilibili.com/486686697
 * @since 2024-06-03 15:23:34
 */
@Service
public class TestTableServiceImpl extends ServiceImpl<TestTableDao, TestTable> implements TestTableService {
    @Resource
    private RedisUtils redisUtils;

    @Override
//    public PageResult<TestTable> pageTestTables(TestTablePageDTO dto) {
    public PageResult<TestTableVO> pageTestTables(TestTablePageDTO dto) {
        IPage<TestTable> testTableIPage=  getBaseMapper().selectPage(new Page<>(dto.getCurrent(),dto.getSize()),
                new LambdaQueryWrapper<TestTable>()

                        .like(StringUtils.hasLength(dto.getUsername()),TestTable::getUsername,dto.getUsername())
        );
        //        return PageResult.getPage(testTableIPage.getRecords(), testTableIPage.getTotal());
        Page<TestTableVO> result= TestTableConverter.instance.toPageConvert(testTableIPage);
        return PageResult.getPage(result.getRecords(),result.getTotal());
    }

    @Override
    public TestLoginVO testLogin(TestLoginDTO dto) {
        TestTable testTable=getOne(new LambdaQueryWrapper<TestTable>().eq(TestTable::getUsername,dto.getUsername()).eq(TestTable::getPassword,dto.getPassword()).last("limit 1"));
        if(testTable==null){
            throw new BusinessException(ResponseCode.DATA_PARAM_ERROR);
        }
        String token = UUID.randomUUID().toString();
        redisUtils.setCacheObject(SystemConstants.getUserOnlineKey(token), testTable.getId(), Duration.ofHours(SystemConstants.TOKEN_EXP));
        TestLoginVO result=new TestLoginVO();
        result.setUsername(testTable.getUsername());
        result.setId(testTable.getId());
        result.setToken(token);
        return result;
    }
}

