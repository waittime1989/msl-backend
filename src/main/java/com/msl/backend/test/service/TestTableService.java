package com.msl.backend.test.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.msl.backend.common.base.PageResult;
import com.msl.backend.test.entity.TestTable;
import com.msl.backend.test.model.dto.TestLoginDTO;
import com.msl.backend.test.model.dto.TestTablePageDTO;
import com.msl.backend.test.model.vo.TestLoginVO;
import com.msl.backend.test.model.vo.TestTableVO;

/**
 * 测试用户表(TestTable)表服务接口
 *
 * @author 明思梨: https://space.bilibili.com/486686697
 * @since 2024-06-03 15:23:34
 */
public interface TestTableService extends IService<TestTable> {
//    PageResult<TestTable> pageTestTables(TestTablePageDTO dto);
    PageResult<TestTableVO> pageTestTables(TestTablePageDTO dto);
    TestLoginVO testLogin(TestLoginDTO dto);
}

