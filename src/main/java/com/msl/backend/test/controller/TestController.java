package com.msl.backend.test.controller;

import com.msl.backend.common.annotation.EnumAnnotation;
import com.msl.backend.common.base.DataResult;
import com.msl.backend.common.base.PageResult;
import com.msl.backend.common.exception.BusinessException;
import com.msl.backend.common.exception.ResponseCode;
import com.msl.backend.test.entity.TestTable;
import com.msl.backend.test.enums.TestValidatorEnum;
import com.msl.backend.test.model.dto.TestLoginDTO;
import com.msl.backend.test.model.dto.TestTablePageDTO;
import com.msl.backend.test.model.dto.ValidObjDTO;
import com.msl.backend.test.model.vo.TestLoginVO;
import com.msl.backend.test.model.vo.TestTableVO;
import com.msl.backend.test.service.TestTableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;


/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/5/21
 */
@RequestMapping("/test")
@RestController
@Api(tags = "测试模块")
@Validated
public class TestController {
    @Value("${branch}")
    private String branch;
    @GetMapping("/hello")
    @ApiOperation(value = "经典Hello World接口")
    public DataResult<String> hello(){
        return DataResult.success("Hello World");
    }
    @ApiOperation(value = "多环境接口")
    @GetMapping("/branch")
    public DataResult<String> getBranch(){
        return DataResult.success(" Spring Boot Current branch "+branch);
    }
    @GetMapping("/testError")
    @ApiOperation(value = "全局异常测试接口")
    public DataResult<String> testError(){
        int i=1/0;
        return DataResult.success("testError");
    }
    @GetMapping("/businessError/{type}")
    @ApiOperation(value = "测试主动抛出业务异常接口")
    public DataResult<String> testBusinessError(@PathVariable("type") String type){
        if(!(type.equals("1")||type.equals("2")||type.equals("3"))){
            throw new BusinessException(ResponseCode.DATA_PARAM_ERROR);
        }
        return DataResult.success(type);
    }
    @PostMapping("/validError")
    @ApiOperation(value = "测试Validator抛出业务异常接口")
    public DataResult<ValidObjDTO> testValid(@RequestBody @Validated ValidObjDTO dto){
        return DataResult.success(dto);
    }

    @GetMapping("/enum-valid")
    @ApiOperation(value = "测试入参加上枚举校验接口")
    public DataResult<String> testEnumValid( @RequestParam(required = false) @EnumAnnotation(value = TestValidatorEnum.class,message = "testValidatorType 类型不正确")  String value){
        return DataResult.success(value);
    }

    @Resource
    private TestTableService testTableService;
    @PostMapping("/page")
    @ApiOperation(value = "测试mybatis plus 分页接口")
    @RequiresPermissions(value = "sys:user:query")
//    public DataResult<PageResult<TestTable>> testPage(@RequestBody @Valid TestTablePageDTO dto){
    public DataResult<PageResult<TestTableVO>> testPage(@RequestBody @Valid TestTablePageDTO dto){
        return DataResult.success(testTableService.pageTestTables(dto));
    }
    @PostMapping("/login")
    @ApiOperation(value = "测试登录接口")
    public DataResult<TestLoginVO> testLogin(@RequestBody @Valid TestLoginDTO dto){
        return DataResult.success(testTableService.testLogin(dto));
    }
}
