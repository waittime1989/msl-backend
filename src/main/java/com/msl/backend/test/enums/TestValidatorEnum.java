package com.msl.backend.test.enums;


import com.msl.backend.common.base.BaseEnum;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/8/13 14:41
 * @description: 测试枚举校验
 */
public enum TestValidatorEnum implements BaseEnum<String> {
    TEST_ONE("1"),
    TEST_TWO("2");
    private final String code;

    TestValidatorEnum(String code) {
        this.code = code;
    }


    @Override
    public String getValue() {
        return code;
    }
}
