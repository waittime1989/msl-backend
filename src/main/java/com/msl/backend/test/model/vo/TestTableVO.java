package com.msl.backend.test.model.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/6/1
 */
@Data
public class TestTableVO {
    @ApiModelProperty("主键ID")
    private Long id;

    @ApiModelProperty("账户名称")
    private String username;


    @ApiModelProperty("创建时间")
    private Date gmtCreate;

    @ApiModelProperty("更新时间")
    private Date gmtModified;
}
