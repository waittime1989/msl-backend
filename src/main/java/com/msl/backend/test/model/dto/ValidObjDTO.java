package com.msl.backend.test.model.dto;

import com.msl.backend.common.annotation.EnumAnnotation;
import com.msl.backend.test.enums.TestValidatorEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/5/31
 */
@Data
public class ValidObjDTO {
    @ApiModelProperty(value = "名称")
    @NotBlank(message = "名称不能为空")
    private String name;

    @NotNull(message = "age 不能为空")
    @Min(value = 0,message = "年龄数据不能为负数")
    @ApiModelProperty(value = "年龄")
    private Integer age;

    @NotEmpty(message = "id 集合不能为空")
    @ApiModelProperty(value = "id集合")
    private List<String> ids;

    @ApiModelProperty(value = "测试枚举属性")
    @EnumAnnotation(value = TestValidatorEnum.class,message = "testValidatorType 类型不正确")
    private String testValidatorType;
}
