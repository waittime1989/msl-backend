package com.msl.backend.test.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/6/3
 */
@Data
public class TestLoginVO {
    @ApiModelProperty("主键ID")
    @JsonSerialize(using= ToStringSerializer.class)
    private Long id;

    @ApiModelProperty("账户名称")
    private String username;
    @ApiModelProperty("token令牌")
    private String token;
}
