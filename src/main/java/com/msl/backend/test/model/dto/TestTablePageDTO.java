package com.msl.backend.test.model.dto;

import com.msl.backend.common.base.BasePage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/6/1
 */
@Data
public class TestTablePageDTO extends BasePage {
    @ApiModelProperty(value = "账号")
    private String username;
}
