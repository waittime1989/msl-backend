package com.msl.backend.test.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description:
 * @date: 2024/6/3
 */
@Data
public class TestLoginDTO {
    @ApiModelProperty(value = "账号")
    @NotBlank(message = "账号不能为空")
    private String username;
    @ApiModelProperty(value = "密码")
    @NotBlank(message = "密码不能为空")
    private String password;
}
