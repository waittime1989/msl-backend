package com.msl.backend.test.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.msl.backend.test.entity.TestTable;

/**
 * 测试用户表(TestTable)表数据库访问层
 *
 * @author 明思梨: https://space.bilibili.com/486686697
 * @since 2024-06-03 15:23:34
 */
public interface TestTableDao extends BaseMapper<TestTable> {

}

