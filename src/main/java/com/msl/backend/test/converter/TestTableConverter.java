package com.msl.backend.test.converter;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.msl.backend.test.entity.TestTable;
import com.msl.backend.test.model.vo.TestTableVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2023/8/13 17:31
 * @description: 分页对象转换器
 */
@Mapper
public interface TestTableConverter {
    TestTableConverter instance= Mappers.getMapper(TestTableConverter.class);
    Page<TestTableVO> toPageConvert(IPage<TestTable> testTableIPage);
}

