package com.msl.backend;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.msl.backend.common.utils.RedisUtils;
import com.msl.backend.test.model.dto.ValidObjDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;



/**
 * @author 明思梨: https://space.bilibili.com/486686697
 * @date 2024/3/26
 * @description: redis 单元测试
 */
@SpringBootTest
@Slf4j
public class RedisTest {
    @Resource
    private RedisUtils redisUtil;
    @Resource
    private ObjectMapper objectMapper;
    @Test
    public void testString(){
        String key = "name";
        redisUtil.setCacheObject(key, "msl");
        String result = redisUtil.getCacheObject(key);
        log.info("testString result:{}",result);
    }
    @Test
    public void testObj(){
        String key = "nameObject";
        ValidObjDTO validObj=new ValidObjDTO();
        validObj.setName("msl");
        validObj.setAge(35);
        redisUtil.setCacheObject(key, validObj);
        ValidObjDTO result = redisUtil.getCacheObject(key);
        try {
            log.info("testString result:{}",objectMapper.writeValueAsString(result));
        } catch (JsonProcessingException e) {
            log.error("testObj error:{}",e.getMessage());
        }
    }
    @Test
    public void testArraysObj(){
        String key = "nameArrays";
        ValidObjDTO validObj=new ValidObjDTO();
        validObj.setName("msl");
        validObj.setAge(35);
        List<ValidObjDTO> list=new ArrayList<>();
        list.add(validObj);
        redisUtil.setCacheObject(key, list);

        try {
            List<ValidObjDTO> result = redisUtil.getCacheList(key);
            log.info("testString result:{}",objectMapper.writeValueAsString(result));
        } catch (JsonProcessingException e) {
            log.error("testArraysObj error:{}",e.getMessage());
        }
    }
}