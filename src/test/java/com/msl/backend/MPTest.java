package com.msl.backend;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.msl.backend.test.entity.TestTable;
import com.msl.backend.test.service.TestTableService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;


/**
 * @author: 明思梨: https://space.bilibili.com/486686697
 * @description: mybatis plus单元测试类
 * @date: 2024/3/27
 */
@SpringBootTest
@Slf4j
public class MPTest {
    @Resource
    private TestTableService testTableService;
    @Test
    public void testInsert(){
        TestTable testTable=new TestTable();
//        testTable.setUsername("admin");
        testTable.setUsername("admin1");
        testTable.setPassword("123456");
        testTableService.save(testTable);
    }
    @Test
    public void testUpdate(){
        TestTable testTable=new TestTable();
        testTable.setPassword("1234567");
//        testTableService.update(testTable,new LambdaQueryWrapper<TestTable>().eq(TestTable::getUsername,"admin"));
        testTableService.update(testTable,new LambdaQueryWrapper<TestTable>().eq(TestTable::getUsername,"admin1"));
    }
}
