# MSL-backend

#### Description
Springboot2.7+Shiro+MyBatis-Plus+redisson+Swagger+EasyExcel+Vue3+TypeScript+Pinia+Element Plus+Axios+Echarts+dexie 前后端分离轻量级、高性能免费开源的快速开发平台。
RBAC权限管理、数据权限、三方登录、分片并发上传、秒传、断点续传、文件在线预览、传输队列支持受到暂停、启动、重试、取消

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
