-- 判断msl_admin库是否存在，存在先删除(如果原先存在数据谨慎操作)
drop database if exists msl_admin;
-- 创建msl_admin
create database msl_admin;
-- 使用msl_admin库
use msl_admin;
-- 判断msl_admin库里面是否存在m_test_table表存在先删除
DROP TABLE IF EXISTS m_test_table;
-- 创建m_test表
CREATE TABLE `m_test_table`
(
    `id`           bigint(20)   NOT NULL COMMENT '主键ID',
    `username`     varchar(50)  NOT NULL COMMENT '账户名称',
    `password`     varchar(200) NOT NULL COMMENT '用户密码',
    `gmt_create`   datetime DEFAULT NULL COMMENT '创建时间',
    `gmt_modified` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `uniq_username` (`username`) USING BTREE COMMENT '唯一索引'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='测试用户表';



